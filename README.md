Repositori GitLab ini memuat paket-paket .alldeb (binary) yang masing-masing berisi sebuah software aplikasi untuk Ubuntu. Paket-paket .alldeb ini didistribusikan di situs http://bengkelubuntu.org. 
Setiap paket .alldeb di sini dipaketkan memakai program skrip alldeb_maker, yang teknologi Alldeb ini dan skrip alldeb_maker itu sendiri didesain dan diciptakan oleh Nifa Dwi Kurniawan.
Paket-paket .alldeb di repositori ini dipaketkan dari paket-paket .deb dari repositori resmi Ubuntu, setiap paketnya ialah free software (free as in freedom), paket .alldeb di sini 
tidak menyertakan proprietary software dan akan dihapus apabila ditemukan. Repositori ini memuat paket-paket .alldeb untuk Ubuntu 16.04 Xenial Xerus 64 bit. 

18 Oktober 2016
Ade Malsasa Akbar <teknoloid@gmail.com>